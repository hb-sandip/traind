import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:kks_demo/ws/HttpResponse.dart';
import 'package:kks_demo/ws/UrlUtils.dart';
import 'package:progress_dialog/progress_dialog.dart';

class MyHttpClient {
  BuildContext context;
  ProgressDialog pr;

  MyHttpClient(this.context);

  Future<HttpResponse> get(String url, dynamic queryParameter) async {
    pr = new ProgressDialog(context, ProgressDialogType.Normal);
    pr.setMessage('Please wait...');
    pr.show();
    try {
      Response res = await getDio().get(url, queryParameters: queryParameter);
      pr.hide();
      return HttpResponse(true, res.data, "");
    } on DioError catch (e) {
      pr.hide();
      return HttpResponse(false, "", e.message);
    }
  }

  Future<HttpResponse> post(String url, dynamic data) async {
    pr = new ProgressDialog(context, ProgressDialogType.Normal);
    pr.setMessage('Please wait...');
    pr.show();
    try {
      Response res = await getDio().post(url, data: data);
      pr.hide();
      return HttpResponse(true, res.data, "");
    } on DioError catch (e) {
      pr.hide();
      return HttpResponse(false, "", e.message);
    }
  }

  Future<HttpResponse> postFormData(String url, FormData data) async {
    pr = new ProgressDialog(context, ProgressDialogType.Normal);
    pr.setMessage('Please wait...');
    pr.show();
    try {
      Response res = await getDio().post(url, data: data);
      pr.hide();
      return HttpResponse(true, res.data, "");
    } on DioError catch (e) {
      pr.hide();
      return HttpResponse(false, "", e.message);
    }
  }

  Dio getDio() {
    Dio dio = new Dio();
    dio.options.baseUrl = UrlUtils.BASE_URL;
    dio.options.connectTimeout = 5000;
    dio.options.receiveTimeout = 5000;
    return dio;
  }
}
