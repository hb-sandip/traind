import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

class MapScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new MapState();
  }
}

class MapState extends State<MapScreen> {
  Completer<GoogleMapController> _controller = Completer();

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(21.1702, 72.8311),
    zoom: 14.4746,
  );

  static List<Marker> m = [
    Marker(
      markerId: MarkerId('1'),
      position: LatLng(21.1702, 72.8311),
    )
  ];

  Set<Marker> markers = Set.from(m);

  static final CameraPosition _kLake = CameraPosition(
      bearing: 192.8334901395799,
      target: LatLng(37.43296265331129, -122.08832357078792),
      tilt: 59.440717697143555,
      zoom: 19.151926040649414);

  PanelController panelController = new PanelController();
  bool slidingPanelOPen = false;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SlidingUpPanel(
      controller: panelController,
      panel: Stack(
        alignment: Alignment.topCenter,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 25),
            child: Column(
              children: <Widget>[
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(color: Color.fromARGB(255, 248, 249, 252)),
                    padding: EdgeInsets.only(top: 25),
                    child: ListView(
                      padding: EdgeInsets.all(10),
                      children: <Widget>[
                        Container(
                          child: Text(
                            'Quick Links',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 15,
                                fontWeight: FontWeight.bold),
                          ),
                          padding: EdgeInsets.all(10.0),
                        ),
                        ListView.builder(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemBuilder: (context, index) {
                            return Card(
                              margin: EdgeInsets.all(10),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    CircleAvatar(
                                      radius: 30.0,
                                      backgroundImage: NetworkImage(
                                          'http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg'),
                                    ),
                                    Expanded(
                                      flex: 1,
                                      child: Container(
                                        margin: EdgeInsets.only(left: 10),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              'Your Last Trainer',
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  color: Color.fromARGB(
                                                      255, 117, 142, 166)),
                                            ),
                                            Text(
                                              'John Trainer',
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  color:
                                                  Color.fromARGB(255, 25, 35, 60),
                                                  fontWeight: FontWeight.bold),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                    FloatingActionButton(
                                      child: Icon(
                                        Icons.keyboard_arrow_right,
                                        color: Colors.white,
                                      ),
                                      backgroundColor: Theme.of(context).primaryColor,
                                    )
                                  ],
                                ),
                              ),
                            );
                          },
                          itemCount: 2,
                        ),
                        Container(
                          child: Text(
                            'Upcomming Sessions',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 15,
                                fontWeight: FontWeight.bold),
                          ),
                          padding: EdgeInsets.all(10.0),
                        ),
                        ListView.builder(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemBuilder: (context, index) {
                            return Card(
                              margin: EdgeInsets.all(10),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    CircleAvatar(
                                      radius: 30.0,
                                      backgroundImage: NetworkImage(
                                          'http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg'),
                                    ),
                                    Expanded(
                                      flex: 1,
                                      child: Container(
                                        margin: EdgeInsets.only(left: 10),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              'Your Last Trainer',
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  color: Color.fromARGB(
                                                      255, 117, 142, 166)),
                                            ),
                                            Text(
                                              'John Trainer',
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  color:
                                                  Color.fromARGB(255, 25, 35, 60),
                                                  fontWeight: FontWeight.bold),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                    FloatingActionButton(
                                      child: Icon(
                                        Icons.keyboard_arrow_right,
                                        color: Colors.white,
                                      ),
                                      backgroundColor: Theme.of(context).primaryColor,
                                    )
                                  ],
                                ),
                              ),
                            );
                          },
                          itemCount: 5,
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
          RaisedButton(
            shape: new RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(25)),
            color: Theme.of(context).primaryColor,
            child: Icon(
              slidingPanelOPen
                  ? Icons.keyboard_arrow_down
                  : Icons.keyboard_arrow_up,
              color: Colors.white,
            ),
            onPressed: () {
              if (panelController.isPanelOpen()) {
                panelController.close();
              } else {
                panelController.open();
              }
            },
          ),
        ],
      ),
      body: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(15.0),
            child: Column(
              children: <Widget>[
                Text(
                  'Good Morning Joe User!',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Color.fromARGB(255, 25, 35, 60),
                      fontSize: 15),
                ),
                Text(
                  "It's a great day to train today!",
                  style: TextStyle(
                    color: Color.fromARGB(255, 117, 142, 166),
                    fontSize: 12,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
              flex: 1,
              child: Stack(
                alignment: Alignment.topCenter,
                children: <Widget>[
                  GoogleMap(
                    mapType: MapType.normal,
                    initialCameraPosition: _kGooglePlex,
                    onMapCreated: (GoogleMapController controller) {
                      _controller.complete(controller);
                      panelController.open();
                    },
                    markers: markers,
                  ),
                  Container(
                    width: 300,
                    margin: EdgeInsets.all(10.0),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(30)),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 15, right: 10),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: TextField(
                              decoration: InputDecoration(
                                  hintText: 'Search...',
                                  hintStyle: TextStyle(
                                      fontSize: 14,
                                      fontStyle: FontStyle.italic),
                                  border: InputBorder.none),
                            ),
                            flex: 1,
                          ),
                          IconButton(
                            icon: Icon(
                              Icons.menu,
                              color: Colors.black,
                            ),
                            onPressed: () {},
                          ),
                          IconButton(
                            icon: Icon(
                              Icons.search,
                              color: Colors.black,
                            ),
                            onPressed: () {},
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ))
        ],
      ),
      color: Color.fromARGB(255, 248, 249, 252),
      minHeight: 50.0,
      maxHeight: 350.0,
      onPanelOpened: () {
        setState(() {
          slidingPanelOPen = true;
        });
      },
      onPanelClosed: () {
        setState(() {
          slidingPanelOPen = false;
        });
      },
      renderPanelSheet: false,
      isDraggable: false,
    );
  }

  Future<void> _goToTheLake() async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(_kLake));
  }
}
