import 'package:flutter/widgets.dart';

class OtherScreen extends StatelessWidget {
  String name;

  OtherScreen(this.name);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Center(
      child: Text(name,style: TextStyle(fontSize: 18),),
    );
  }
}
