import 'package:flutter/material.dart';
import 'package:kks_demo/screen/MapScreen.dart';

import 'OtherScreen.dart';

class HomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new HomeState();
  }
}

class HomeState extends State<HomeScreen> {
  int menuIndex = 0;

  List<Widget> screens = [
    MapScreen(),
    OtherScreen('Menu'),
    OtherScreen('Favourite'),
    OtherScreen('Message')
  ];

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: new Image.asset(
          'images/icon_app.png',
          height: 35,
        ),
        leading: BackButton(),
        actions: <Widget>[
          IconButton(
            icon: Image.asset(
              'images/icon_setting.png',
              height: 25,
            ),
            onPressed: () {},
          ),
        ],
      ),
      body: screens[menuIndex],
      bottomNavigationBar: new BottomNavigationBar(
          currentIndex: menuIndex,
          type: BottomNavigationBarType.fixed,
          onTap: (int index) {
            setState(() {
              this.menuIndex = index;
            });
          },
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: new ImageIcon(AssetImage('images/icon_profile.png')),
              title: new Padding(padding: EdgeInsets.all(0)),
            ),
            BottomNavigationBarItem(
              icon: new ImageIcon(AssetImage('images/icon_menu.png')),
              title: new Padding(padding: EdgeInsets.all(0)),
            ),
            BottomNavigationBarItem(
              icon: new ImageIcon(AssetImage('images/icon_favourite.png')),
              title: new Padding(padding: EdgeInsets.all(0)),
            ),
            BottomNavigationBarItem(
              icon: new ImageIcon(AssetImage('images/icon_message.png')),
              title: new Padding(padding: EdgeInsets.all(0)),
            ),
          ]),
    );
  }
}
